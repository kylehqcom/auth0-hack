# Auth0 Hack

This project was built as a test case for [http://auth0.com](http://auth0.com/)

The idea being that a `wt` cron job will search Twitter checking for the hash tag of `auth0` (though any hash tag can be passed in) and calculate the total times the hash tag was used. The total is then inserted as a new row in a Google sheet via [https://ifttt.com](https://ifttt.com). Note that the since time used to make Twitter requests is UTC based.
 
 If you so desire, you can also just browse to the public `wt` url which will return a json payload of the hash tag, date, and total.
  
 ![](./resources/web.png)
 

## Pre-requisites

Be sure you have `wt` running on your local. All the details can be found at [https://webtask.io](https://webtask.io)

Setup a Twitter app via [https://apps.twitter.com](https://apps.twitter.com) as this repo needs the Consumer and Access Token/Keys. 

You will also need a [https://ifttt.com](https://ifttt.com) account as this repo requires a maker key and maker event. More can be found at [https://ifttt.com/services/maker_webhooks/settings](https://ifttt.com/services/maker_webhooks/settings).

This code assumes you have a google account for the hash tag totals to be stored inside a Google sheet. Although you can alter the `ifttt applet` to store anywhere you choose!

## Usage

Once the codebase is checked out, locally you can serve or create the `webtask` app with 
 
```
wt serve|create \
--secret CONSUMER_KEY=XXXXXXXXXXXXXXXXXXXXXXXXX \
--secret CONSUMER_SECRET=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
--secret ACCESS_TOKEN=XXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
--secret ACCESS_TOKEN_SECRET=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
--secret MAKER_EVENT=XXXX \
--secret MAKER_KEY=XXXXXXXXXXXXXXXXXXXXXX \
./auth0.js
```

Though it may be best that you instead use the `webtask` online editor using `wt edit`. This way you can add your `wt secrets` directly into the cron task. Again it's your discretion as to how many times you want your cron to poll, but remember that Twitter will rate limit too many requests.

___

![](./resources/cron.png)

With your cron all setup you will need to create your own `ifttt applet` using a `maker webhook trigger`. Be sure you set your `MAKER_EVENT` and `MAKER_KEY` as a secret in the cron tab too! You can then proceed to test your maker web hook.

___

![](./resources/maker-webhook.png)

All going well, each cron event will send a http request to your maker webhook. You should now have entries in your Google sheet!

 ___
 
![](./resources/google-sheet.png)
