var Twitter = require('twit'),
    whilst = require('async').whilst,
    qs = require('qs'),
    request = require('request'),
    searchParams = {}
;

// Return a new client for http requests.
function new_client(ctx) {
    return new Twitter({
        consumer_key: ctx.data.CONSUMER_KEY,
        consumer_secret: ctx.data.CONSUMER_SECRET,
        access_token: ctx.data.ACCESS_TOKEN,
        access_token_secret: ctx.data.ACCESS_TOKEN_SECRET,
        timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
    })
}

// Perform the search returning the length of results and any next url to page via the callback.
function search(client, params, cb) {
    console.log("Requesting:", params.q);
    client.get('search/tweets', params, function (err, data, response) {
        var len = 0,
            next = ""
        ;

        if (err) {
            console.log("Error:", err.message);
        } else {
            if (undefined !== data.statuses) {
                len = data.statuses.length;
                if (undefined !== data.search_metadata.next_results) {
                    next = data.search_metadata.next_results
                }
            }
        }
        cb(len, next);
    });
}

// Set the search query string.
function setSearchQuery(queryString) {
    searchParams = {q: queryString, count: 100};
    return searchParams;
}

module.exports = function (ctx, done) {
    var client = new_client(ctx),
        ht = ctx.data.ht,
        total = 0,
        now = new Date(),
        since = now.getUTCFullYear() + '-' + (now.getUTCMonth() + 1) + '-' + (now.getUTCDate()),
        page = true
    ;

    if (undefined === ht || "" === ht) {
        ht = "auth0";
    }
    setSearchQuery('#' + ht + " since:" + since);

    whilst(
        function () {
            return page
        },
        function (callback) {
            search(client, searchParams, function (count, next) {
                total += count;
                if ("" == next) {
                    page = false
                } else {
                    var parsed = qs.parse(next.substring(1));
                    setSearchQuery('#' + ht + " since:" + since + " max_id:" + parsed.max_id);
                }
                callback();
            })
        },
        function (err, n) {
            var options = {
                method: 'POST',
                url: 'https://maker.ifttt.com/trigger/' + ctx.data.MAKER_EVENT + '/with/key/' + ctx.data.MAKER_KEY,
                headers: {
                    'cache-control': 'no-cache',
                    'content-type': 'multipart/form-data;'
                },
                formData: {value1: ht, value2: since, value3: total}
            };
            request(options);
            console.log("Total results:", total);
            done(null, {"hash_tag": ht, "total": total, "since": since});
        }
    );
}
